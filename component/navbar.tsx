import style from './css/Navbar.module.css';

export default function navbar() {
    return (
        <>
            <div className={style.container}>
                <nav>
                    <ul>
                        <li className={style.menuButton}>
                            <i className="fa fa-bars" aria-hidden="true"></i>
                        </li>
                        <li className={style.logo}>
                            <a href="">MyDramaIndo</a>
                        </li>
                        <li>
                            <form>
                                <input className={style.searchBar} type="search" placeholder="Search" aria-label="Search" />
                            </form>
                        </li>
                        <li>
                            <i className="fa fa-chevron-right" aria-hidden="true"></i>
                        </li>
                        <li>
                            <div className={style.filter}>
                                <a href="">Filter Pencarian <i className="fa fa-cog" aria-hidden="true"/></a>
                            </div>
                        </li>
                    </ul>
                </nav> 
            </div>
        </>
    )
}

export async function getServerSideProps() {
    const settings = {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'test',
        }
    }
    
    const res = await fetch('https://anime.berangkasilmu.com/example/?page=menu', settings);
    const menu = await res.json();
    console.log(menu.data)
    return {
      props: {
          menu
      }, 
    }
  }
