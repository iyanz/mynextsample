import "materialize-css/dist/css/materialize.min.css";
import Header from '../component/header';


interface blogPostProps{
    id: number,
    title: string,
    body: string
}

export default function anime(props:blogPostProps) {
    const {blogPost} = props;
    return (
        <>
        <Header />
            <div className="card-panel teal lighten-2"><h3>{blogPost.title}</h3></div>
            <small>{blogPost.synopsis}</small>
        <Footer />
        </>
    )
}

export async function getStaticProps() {
    const res = await fetch('https://berkasin.my.id/mal/?id=38816');
    const blogPost = await res.json();
    return {
        props: {
            blogPost,
        }
    }
}